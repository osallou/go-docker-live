0.1.4:
  Track god.err with god.log, append [ERROR] to err messages
0.1.3:
  Fix option useWatchFile to support nfs change detection
0.1.2:
  Auth redis authentication support, add "redis/password" parameter in config, defaults to null (no auth)

0.1.1:
  Fix #2, upgrade tail npm package to enable tailing of files updated on remote location

0.1: First release
