
var cli = require('cli');
var fs = require('fs');
var config = require('config');
var redis = require('redis');
var io = require('socket.io');
var http = require('http');
var express = require('express');
var jwt = require('jsonwebtoken');
Tail = require('tail').Tail

var winston = require('winston');
winston.emitErrs = true;

var logger = new winston.Logger({
    transports: [
        new winston.transports.File({
            level: 'info',
            filename: 'god_live.log',
            handleExceptions: true,
            json: true,
            maxsize: 5242880, //5MB
            maxFiles: 5,
            colorize: false
        }),
        new winston.transports.Console({
            level: 'debug',
            handleExceptions: true,
            json: false,
            colorize: true
        })
    ],
    exitOnError: false
});

var redis_client = null;
if(config.has('redis.password') && config.get('redis.password')) {
    logger.info("Use redis authentication");
    redis_client = redis.createClient(config.get('redis.port'), config.get('redis.host'), {
        password: config.get('redis.password'),
        auth_pass: config.get('redis.password')
    }); //creates a new client
}
else {
    logger.info("no auth");
    redis_client = redis.createClient(config.get('redis.port'), config.get('redis.host')); //creates a new client
}

var clients = {};
var tails = {};

cli.parse({
    log:   ['l', 'Enable logging'],
});


cli.main(function(args, options) {

    logger.info("Listen on: "+config.get('app.port'));
    logger.info("Redis: "+config.get('redis.host')+":"+config.get('redis.port'));
    logger.info("Subscription channel: "+config.get('redis.prefix')+':jobs:pubsub')

    var app = express();
    var server = require('http').Server(app);
    var io = require('socket.io')(server);
    server.listen(config.get('app.port'));
    app.use(express.static(__dirname + '/public'));
    app.get('/', function (req, res) {
        res.sendFile(__dirname + '/index.html');
    });
    app.get('/public/bower_components/socket.io-client/socket.io.js', function (req, res){
        res.sendFile(__dirname + '/public/bower_components/socket.io-client/socket.io.js');
    });

    io.on('connection', function (socket) {
      socket.on('message',function(event){
          if(event.type == 'untail'){
            if(tails[socket.id] != undefined && tails[socket.id]!=null) {
                tails[socket.id].unwatch();
            }
            delete  tails[socket.id];
          }
          if(event.type == 'tail') {
              if(tails[socket.id] != undefined && tails[socket.id]!=null) {
                  tails[socket.id].unwatch();
              }
              var token = event.token;
              var decoded_msg = jwt.verify(token['token'], config.get('app.secret_passphrase'));
              var task = decoded_msg['task'];
              //logger.debug(task);
              var fileName = null;
              var errFileName= null;
              for(var i=0;i<task['container']['volumes'].length;i++){
                  if(task['container']['volumes'][i]['name'] == 'go-docker'){
                      fileName = task['container']['volumes'][i]['path']+'/god.log';
                      errFileName = task['container']['volumes'][i]['path']+'/god.err';
                      break;
                  }
              }
              if(fileName !== null){
                  fs.readFile(fileName, 'utf8', function (err,data) {
                      if (! err) {
                          io.to(socket.id).emit('first-data',data);
                      }
                      var options = {separator: /[\r]{0,1}\n/, fromBeginning: true, follow: true, useWatchFile: true};
                      var tail = new Tail(fileName, options);
                      tail.on('line', function(data) {
                          io.to(socket.id).emit('new-data',data);
                      });
                      tails[socket.id] = tail;
                  });
                  fs.readFile(errFileName, 'utf8', function (err,data) {
                      if (!err && data) {
                          io.to(socket.id).emit('first-data','[ERROR] ' + data);
                      }
                      var options = {separator: /[\r]{0,1}\n/, fromBeginning: true, follow: true, useWatchFile: true};
                      var tail = new Tail(errFileName, options);
                      tail.on('line', function(data) {
                          if(data){
                              io.to(socket.id).emit('new-data','[ERROR] ' + data);
                          }
                      });
                      tails[socket.id] = tail;
                  });


              }
          }
          if(event.type == 'authenticate'){
              // Associate channel to user
              socket.username = event.user;
              clients[event.user] = socket.id;
          }
          logger.debug('Received message from client:',event.type);
      });
    });
    io.on('connect', function(socket){
        logger.debug('new connection');
    });

    io.on('disconnect',function(socket){
        logger.debug(socket.username+' disconnected');
        delete clients[socket.username];
        tails[socket.id].unwatch();
    });


    redis_client.on("subscribe", function (channel, count) {
        logger.debug("Subscribed");
    });


    redis_client.on('connect', function() {
        logger.info('Redis connected');
    });

    redis_client.on("error", function (err) {
        logger.error("Redis error: " + err);
        process.exit(1);
    });

    redis_client.subscribe(config.get('redis.prefix')+':jobs:pubsub');

    redis_client.on("message", function(channel, message) {
        var task = JSON.parse(message);
        if(clients[task.user] !== undefined) {
            logger.debug("Send to user "+task.user)
            io.to(clients[task.user]).emit('taskover', task);
        }
        else {
            logger.debug(task.user+" not present in connected clients");
        }
    });


});
