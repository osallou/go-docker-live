var ws = require("nodejs-websocket");
var cli = require('cli');
var fs = require('fs');
var config = require('config');
var redis = require('redis');
var redis_client = redis.createClient(config.get('redis.port'), config.get('redis.host')); //creates a new client

var winston = require('winston');
winston.emitErrs = true;

var logger = new winston.Logger({
    transports: [
        new winston.transports.File({
            level: 'info',
            filename: 'god_live.log',
            handleExceptions: true,
            json: true,
            maxsize: 5242880, //5MB
            maxFiles: 5,
            colorize: false
        }),
        new winston.transports.Console({
            level: 'debug',
            handleExceptions: true,
            json: false,
            colorize: true
        })
    ],
    exitOnError: false
});

cli.parse({});


cli.main(function(args, options) {

logger.info("Redis: "+config.get('redis.host')+":"+config.get('redis.port'));

redis_client.publish(config.get('redis.prefix')+':jobs:pubsub',
JSON.stringify({'name': 'test', 'user': 'osallou', 'id': 1, 'status': 'killed'}))


redis_client.on('connect', function() {
    logger.info('Redis connected');
});


});
